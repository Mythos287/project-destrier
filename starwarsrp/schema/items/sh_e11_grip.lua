ITEM.name = "E-11 Grip"
ITEM.category = "Attachment"
ITEM.desc = "A foregrip for the E-11 Blaster Rifle."
ITEM.model = "models/sw_battlefront/props/e11_grip/e11_grip.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(5, 0, 30),
	ang = Angle(90, 0, 0),
	fov = 45,
}
