ITEM.name = "E-11 Ironsights"
ITEM.category = "Attachment"
ITEM.desc = "A set of ironsights for the E-11 Blaster Rifle."
ITEM.model = "models/sw_battlefront/props/e11r_scope/e11r_scope.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(5, 0, 30),
	ang = Angle(90, 0, 0),
	fov = 45,
}
