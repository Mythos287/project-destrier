ITEM.name = "E-11R Stock"
ITEM.category = "Attachment"
ITEM.desc = "An extending stock for the E-11 Blaster Rifle."
ITEM.model = "models/sw_battlefront/props/e11r_stock/e11r_stock.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(5, 0, 30),
	ang = Angle(90, 0, 0),
	fov = 45,
}
