ITEM.name = "T-21B"
ITEM.desc = "A T-21B Heavy Blaster Rifle with two optics mounted. It is not recommended for close-quarters."
ITEM.model = "models/strasser/weapons/t21b/t21b.mdl"
ITEM.class = "bf2_t21b"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
