ITEM.name = "Dual E-11"
ITEM.desc = "A pair E-11 Light Blaster Rifles."
ITEM.model = "models/swbfii/weapons/e11.mdl"
ITEM.class = "rw_sw_dual_e11"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
