ITEM.name = "DL-44"
ITEM.desc = "A DL-44 Heavy Blaster Pistol by BlasTech Industries."
ITEM.model = "models/strasser/weapons/dl44/dl44.mdl"
ITEM.class = "bf2017_dl44"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
