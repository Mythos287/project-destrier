ITEM.name = "A280"
ITEM.desc = "An A280 Blaster Rifle by Blastech Industries."
ITEM.model = "models/strasser/weapons/a280/a280.mdl"
ITEM.class = "bf2017_a280"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
