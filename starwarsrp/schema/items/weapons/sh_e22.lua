ITEM.name = "E-22"
ITEM.desc = "An E-22 Double-Barreled Blaster by BlasTech Industries"
ITEM.model = "models/sw_battlefront/weapons/ven_e22.mdl"
ITEM.class = "ven_e22"
ITEM.weaponCategory = "primary"
ITEM.width = 5
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
