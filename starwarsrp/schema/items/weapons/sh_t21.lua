ITEM.name = "T-21"
ITEM.desc = "A T-21 Heavy Repeating Blaster Rifle. Deals massive damage at long-ranges, but has a low fire-rate."
ITEM.model = "models/strasser/weapons/t21/t21.mdl"
ITEM.class = "bf2_t21"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
