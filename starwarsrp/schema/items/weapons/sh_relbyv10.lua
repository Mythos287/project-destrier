ITEM.name = "Relby-V10"
ITEM.desc = "A Relby-V10 Grenade Launcher modified to act as a Blaster Rifle."
ITEM.model = "models/strasser/weapons/relbyv10/relbyv10.mdl"
ITEM.class = "bf2017_relbyv10"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
