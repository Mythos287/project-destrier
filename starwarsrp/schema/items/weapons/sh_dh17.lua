ITEM.name = "DH-17"
ITEM.desc = "A DH-17 Blaster Pistol by BlasTech Industries. Capable of medium-range engagements."
ITEM.model = "models/strasser/weapons/dh17/dh17.mdl"
ITEM.class = "bf2017_dh17"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
