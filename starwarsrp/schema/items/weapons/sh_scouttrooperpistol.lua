ITEM.name = "Scout Trooper Pistol"
ITEM.desc = "A Hold-Out Blaster Pistol that is the standard issue of Scout Troopers."
ITEM.model = "models/strasser/weapons/scouttrooper/scouttrooper.mdl"
ITEM.class = "bf2_scouttrooper"
ITEM.weaponCategory = "sidearm"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
