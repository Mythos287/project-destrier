ITEM.name = "DLT-19"
ITEM.desc = "A DLT-19 Heavy Blaster Rifle by BlasTech Industries featuring a high fire-rate."
ITEM.model = "models/strasser/weapons/dlt19/dlt19.mdl"
ITEM.class = "bf2_dlt19"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
