ITEM.name = "DC-17"
ITEM.desc = "A DC-17 Blaster Pistol by BlasTech Industries."
ITEM.model = "models/strasser/weapons/dc17/dc17.mdl"
ITEM.class = "bf2017_dc177"
ITEM.weaponCategory = "melee"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
