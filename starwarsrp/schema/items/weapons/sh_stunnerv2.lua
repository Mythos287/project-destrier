ITEM.name = "Stunner V2"
ITEM.desc = "A stun-baton utilized for self-defense, discipline, and law enforcement."
ITEM.model = "models/weapons/w_stunbaton.mdl"
ITEM.class = "weapon_stunner2"
ITEM.weaponCategory = "melee"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
