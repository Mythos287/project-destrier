ITEM.name = "DC-15A"
ITEM.desc = "A DC-15A Blaster Rifle by BlasTech Industries."
ITEM.model = "models/strasser/weapons/dc15a/dc15a.mdl"
ITEM.class = "bf2017_dc15aa"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
