ITEM.name = "EE-3"
ITEM.desc = "An EE-3 Blaster Carbine by BlasTech Industries capable of long-range engagements."
ITEM.model = "models/strasser/weapons/ee3/ee3.mdl"
ITEM.class = "bf2017_ee3"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
