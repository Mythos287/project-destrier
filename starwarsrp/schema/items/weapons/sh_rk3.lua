ITEM.name = "RK-3"
ITEM.desc = "An RK-3 Blaster Pistol"
ITEM.model = "models/strasser/weapons/rk3/rk3.mdl"
ITEM.class = "bf2_rk3"
ITEM.weaponCategory = "sidearm"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
ITEM.functions.Combine = {
    name = "Combine",
    tip = "combineTip",
    icon = "icon16/tick.png",
    onRun = function(item)
		local position = item.player:getItemDropPos()
		local client = item.player

		timer.Simple(0, function()
			for k, v in pairs(item.resultItem) do
				if (
					IsValid(client) and
					client:getChar() and
					not client:getChar():getInv():add(v)
				) then
					nut.item.spawn(v, position)
				end
			end
		end)
		
	end
}
ITEM.resultItem = {"dual_rk3"}