ITEM.name = "Dual RK-3"
ITEM.desc = "A pair of RK-3 Blaster Pistols"
ITEM.model = "models/strasser/weapons/rk3/rk3.mdl"
ITEM.class = "rw_sw_dual_rk3"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
ITEM.functions.Separate = {
    name = "Separate",
    tip = "separateTip",
    icon = "icon16/tick.png",
    onRun = function(item)
		local position = item.player:getItemDropPos()
		local client = item.player

		timer.Simple(0, function()
			for k, v in pairs(item.resultItem) do
				if (
					IsValid(client) and
					client:getChar() and
					not client:getChar():getInv():add(v)
				) then
					nut.item.spawn(v, position)
				end
			end
		end)

	end
}
ITEM.resultItem = {"rk3", "rk3"}