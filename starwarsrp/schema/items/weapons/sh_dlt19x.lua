ITEM.name = "DLT-19X"
ITEM.desc = "A DLT-19X Heavy Blaster Rifle by BlasTech Industries. Unlike the standard DLT-19, the X series has been modified for sniping."
ITEM.model = "models/strasser/weapons/dlt19x/dlt19x.mdl"
ITEM.class = "bf2_dlt19x"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
