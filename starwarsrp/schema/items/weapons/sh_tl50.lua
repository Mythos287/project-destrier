ITEM.name = "TL-50"
ITEM.desc = "A TL-50 Heavy Repeating Blaster"
ITEM.model = "models/strasser/weapons/tl50/tl50.mdl"
ITEM.class = "bf2_tl50"
ITEM.weaponCategory = "primary"
ITEM.width = 4
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
