ITEM.name = "E-11"
ITEM.desc = "An E-11 Light Blaster Rifle by BlasTech Industries"
ITEM.model = "models/bf2017/ven/w_e11.mdl"
ITEM.class = "ven_e11"
ITEM.weaponCategory = "primary"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
