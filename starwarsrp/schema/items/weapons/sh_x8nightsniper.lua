ITEM.name = "X-8 Night Sniper"
ITEM.desc = "An X-8 Night Sniper Blaster Pistol by BlasTech Industries."
ITEM.model = "models/strasser/weapons/x8/x8.mdl"
ITEM.class = "bf2017_x8"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
