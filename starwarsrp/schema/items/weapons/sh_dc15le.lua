ITEM.name = "DC-15LE"
ITEM.desc = "A DC-15LE Blaster Rifle by BlasTech Industries. Modified for long-range engagements, and firing explosive bolts."
ITEM.model = "models/strasser/weapons/dc15le/dc15le.mdl"
ITEM.class = "bf2017_dc15lee"
ITEM.weaponCategory = "primary"
ITEM.width = 6
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
