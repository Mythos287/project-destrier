ITEM.name = "Valken-38x"
ITEM.desc = "A Valken-38x Long-Blaster for high precision and power over long-ranges."
ITEM.model = "models/strasser/weapons/valken/valken.mdl"
ITEM.class = "bf2017_valkenn"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
