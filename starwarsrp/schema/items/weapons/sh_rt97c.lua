ITEM.name = "RT-97C"
ITEM.desc = "An RT-97C Heavy Blaster Rifle capable of long-range engagements. Fitted with a beta magazine, and sporting a high fire-rate."
ITEM.model = "models/strasser/weapons/rt97c/rt97c.mdl"
ITEM.class = "bf2017_rt97c"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
