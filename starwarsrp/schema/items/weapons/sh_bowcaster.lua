ITEM.name = "Bowcaster"
ITEM.desc = "A traditional bowcaster handcrafted by the Wookiees of Kashyyyk."
ITEM.model = "models/strasser/weapons/bowcaster/bowcaster.mdl"
ITEM.class = "bf2017_bowcaster"
ITEM.weaponCategory = "primary"
ITEM.width = 2
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
