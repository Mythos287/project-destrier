ITEM.name = "Defender Sporting"
ITEM.desc = "A Defender Sporting Pistol by the Drearian Defense Conglomerate. A low-powered weapon meant for civilian self-defense, hunting, and duels."
ITEM.model = "models/strasser/weapons/defender/defender.mdl"
ITEM.class = "bf2017_defender"
ITEM.weaponCategory = "sidearm"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-17.581502914429, 250.7974395752, 0),
	fov	= 5.412494001838,
	pos	= Vector(57.109928131104, 181.7945098877, -60.738327026367)
}
