ITEM.name = "E-11 Scope"
ITEM.category = "Attachment"
ITEM.desc = "A scope for the E-11 Blaster Rifle with 4x magnification."
ITEM.model = "models/sw_battlefront/props/e11_scope/e11_scope.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(5, 0, 30),
	ang = Angle(90, 0, 0),
	fov = 45,
}
