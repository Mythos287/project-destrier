ITEM.name = "E-11 Flashlight"
ITEM.category = "Attachment"
ITEM.desc = "A flashlight for the E-11 Blaster Rifle."
ITEM.model = "models/sw_battlefront/props/flashlight/flashlight.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(5, 0, 30),
	ang = Angle(90, 0, 0),
	fov = 45,
} 
