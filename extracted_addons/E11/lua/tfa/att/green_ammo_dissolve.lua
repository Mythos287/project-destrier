if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Yellow Tibanna Gas Cartridges"
ATTACHMENT.ShortName = "YTG" --Abbreviation, 5 chars or less please
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { 
TFA.AttachmentColors["="],"High energy ammo type - Single Shot. Meant to be used in place of the standard GTGC. Intended for single shot assassination.",
TFA.AttachmentColors["+"],"%400 Damage increase",
TFA.AttachmentColors["+"],"Disolve on kill",
TFA.AttachmentColors["+"],"Green Tracer",
TFA.AttachmentColors["-"],"%50 decrease to Clipsize",
TFA.AttachmentColors["-"],"%100 increase to ammo consumption",
TFA.AttachmentColors["-"],"LARGE DELAY ON RELOAD! DANGER!",
}
ATTACHMENT.Icon = "entities/tfa_ammo_fragshell.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.Damage = 60.5

ATTACHMENT.WeaponTable = {
	["Primary"] = {
		["Damage"] = function( wep, val) return val * 5 end,
		["ClipSize"] = 75,
		["DefaultClip"] = 75,
		["RPM"] = 30,
		["RPM_Burst"] = function( wep, val) return val * 0.75 end,
		["Sound"] = "weapons/bf3/e11_heavy.wav",
		["DamageTypeHandled"] = true,
		["DamageType"] = DMG_DISSOLVE,
		--["DamageType"] = DMG_BLAST,
		--["ImpactEffect"] = "Explosion",
		--["ImpactDecal"] = "Explosion",
		["AmmoConsumption"] = 75,
		["Ammo"] = "RPG_Round"
	},
	
	["TracerName"] = "effect_sw_laser_yellow"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end