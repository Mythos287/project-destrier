if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Blue Tibanna Gas Cartridges"
ATTACHMENT.ShortName = "BTGC" --Abbreviation, 5 chars or less please
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = { 
TFA.AttachmentColors["="],"Low energy ammo type - Fast rate of fire, low impact.",
TFA.AttachmentColors["+"],"%50 increase to Clip Size",
TFA.AttachmentColors["+"],"%50 increase to RPM",
TFA.AttachmentColors["+"],"%25 increase to movement speed.",
TFA.AttachmentColors["-"],"%70 decrease to damage",
TFA.AttachmentColors["-"],"%200 decrease to accuracy",
}
ATTACHMENT.Icon = "entities/tfa_ammo_fragshell.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.Damage = 15.5

ATTACHMENT.WeaponTable = {
	["Primary"] = {
		["Damage"] = 8,
		["ClipSize"] = 48,
		["DefaultClip"] = 48,
		["RPM"] = 550,
		["RPM_Burst"] = function( wep, val) return val * 0.75 end,
		["Spread"] = function( wep, val) return val * 2 end,
		["IronAccuracy"] = function( wep, val) return val * 10 end,
		["Force "] = 500,
		["Sound"] = "weapons/bf3/e11_light.wav"
	},
	["MoveSpeed"] = 1.05,
	["TracerName"] = "effect_sw_laser_blue"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end