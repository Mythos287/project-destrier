SWEP.Gun							= ("ven_e11")
if (GetConVar(SWEP.Gun.."_allowed")) != nil then
	if not (GetConVar(SWEP.Gun.."_allowed"):GetBool()) then SWEP.Base = "tfa_blacklisted" SWEP.PrintName = SWEP.Gun return end
end
SWEP.Base							= "tfa_gun_base" // tfa_gun_base
SWEP.Category						= "TFA Star Wars In-Development"
SWEP.Manufacturer 					= ""
SWEP.Author							= "Servius(code), Venator(model)"
SWEP.Contact						= ""
SWEP.Spawnable						= true
SWEP.AdminSpawnable					= true
SWEP.DrawCrosshair					= true
SWEP.DrawCrosshairIS 				= false
SWEP.PrintName						= "E-11"
SWEP.Type							= "Precision Blaster Rifle"
SWEP.DrawAmmo						= true
SWEP.data 							= {}
SWEP.data.ironsights				= 1
SWEP.Secondary.IronFOV				= 78
SWEP.Slot							= 3
SWEP.SlotPos						= 5


SWEP.FiresUnderwater 				= true

SWEP.IronInSound 					= nil
SWEP.IronOutSound 					= nil
SWEP.CanBeSilenced					= false
SWEP.Silenced 						= false
SWEP.DoMuzzleFlash 					= false
SWEP.SelectiveFire					= true
SWEP.DisableBurstFire				= false
SWEP.OnlyBurstFire					= false
SWEP.DefaultFireMode 				= "auto"
SWEP.FireModeName 					= nil
SWEP.DisableChambering 				= true

SWEP.Primary.ClipSize				= 28
SWEP.Primary.DefaultClip			= 56
SWEP.Primary.RPM					= 225
SWEP.Primary.RPM_Burst				= 225
SWEP.Primary.Ammo					= "ar2"
SWEP.Primary.AmmoConsumption 		= 1
SWEP.Primary.Range 					= 10000
SWEP.Primary.RangeFalloff 			= -1
SWEP.Primary.NumShots				= 1
SWEP.Primary.Automatic				= true
SWEP.Primary.RPM_Semi				= nil
SWEP.Primary.BurstDelay				= .1
SWEP.Primary.Sound 					= Sound ("weapons/bf3/e11_a.wav");
SWEP.Primary.ReloadSound 			= Sound ("weapons/bf3/standard_reload2.ogg");
SWEP.Primary.PenetrationMultiplier 	= 0
SWEP.Primary.Damage					= 32.5
SWEP.Primary.HullSize 				= 0
SWEP.DamageType 					= nil

SWEP.DoMuzzleFlash 					= true
SWEP.CustomMuzzleFlash 				= true
SWEP.MuzzleFlashEffect 				= "tfa_muzzleflash_incendiary"

SWEP.FireModes = {
	"Automatic",
	"Single",
}

SWEP.IronRecoilMultiplier			= 0.01
SWEP.CrouchRecoilMultiplier			= 0.01
SWEP.JumpRecoilMultiplier			= 0.01
SWEP.WallRecoilMultiplier			= 0.02
SWEP.ChangeStateRecoilMultiplier	= 0.02
SWEP.CrouchAccuracyMultiplier		= 0.8
SWEP.ChangeStateAccuracyMultiplier	= 1
SWEP.JumpAccuracyMultiplier			= 10
SWEP.WalkAccuracyMultiplier			= 1.8
SWEP.NearWallTime 					= 0.5
SWEP.ToCrouchTime 					= 0.25
SWEP.WeaponLength 					= 35
SWEP.SprintFOVOffset 				= 12
SWEP.ProjectileVelocity 			= 9

SWEP.ProjectileEntity 				= nil
SWEP.ProjectileModel 				= nil

SWEP.ViewModel = "models/ven/sw_battlefront/weapons/bf2017/v_e11.mdl"
SWEP.WorldModel = "models/bf2017/ven/w_e11.mdl"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip					= false
SWEP.MaterialTable 					= nil
SWEP.UseHands 						= true
SWEP.HoldType 						= "ar2"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true

SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(0,-1,-0.00)
SWEP.BlowbackCurrentRoot			= 0
SWEP.BlowbackCurrent 				= 0
SWEP.BlowbackBoneMods 				= nil
SWEP.Blowback_Only_Iron 			= true
SWEP.Blowback_PistolMode 			= false
SWEP.Blowback_Shell_Enabled 		= false
SWEP.Blowback_Shell_Effect 			= "None"

SWEP.Tracer							= 0
SWEP.TracerName 					= "effect_sw_laser_red"
SWEP.TracerCount 					= 1
SWEP.TracerLua 						= false
SWEP.TracerDelay					= 0.01
SWEP.ImpactEffect 					= "effect_sw_impact"
SWEP.ImpactDecal 					= "FadingScorch"

SWEP.VMPos = Vector(0.5, -2, .5)
SWEP.VMAng = Vector(0,0,0)

SWEP.IronSightTime 					= 0.5
SWEP.Primary.KickUp					= 0.03
SWEP.Primary.KickDown				= 0.01
SWEP.Primary.KickHorizontal			= 0.055
SWEP.Primary.StaticRecoilFactor 	= 0.01
SWEP.Primary.Spread					= .02
SWEP.Primary.IronAccuracy 			= .001
SWEP.Primary.SpreadMultiplierMax 	= 1.5
SWEP.Primary.SpreadIncrement 		= 0.35
SWEP.Primary.SpreadRecovery 		= 0.98
SWEP.DisableChambering 				= true
SWEP.MoveSpeed 						= 1
SWEP.IronSightsMoveSpeed 			= 0.75

SWEP.IronSightsPos = Vector(-3.9, -6, 1.75) //across //close //up&down
SWEP.IronSightsAng = Vector(-.18, 0, 0)
SWEP.RunSightsPos = Vector(4, -2, 1.5)
SWEP.RunSightsAng = Vector(-28, 42, -25)
SWEP.InspectPos = Vector(8, -5, -3)
SWEP.InspectAng = Vector(14, 48, 0)


SWEP.WorldModelBoneMods = {
	["ATTACH_Muzzle"] = { scale = Vector(0.5, 0.5, 0.5), pos = Vector(7.025, 0.075, 2.475), angle = Angle(0, 0, 0) },
}

SWEP.WElements = {
	["scope"] = { type = "Model", model = "models/sw_battlefront/props/e11_scope/e11_scope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.299, 1.379, -5.301), angle = Angle(-11, 0, 176), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["flashlight"] = { type = "Model", model = "models/sw_battlefront/props/flashlight/flashlight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11, -1.101, -4.676), angle = Angle(-12, 0, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["powerpack"] = { type = "Model", model = "models/sw_battlefront/props/powerpack/power_pack.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.599, 2.9, -4.7), angle = Angle(180, 90, -10.52), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11_grip"] = { type = "Model", model = "models/sw_battlefront/props/e11_grip/e11_grip.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6.75, 1.2, -3), angle = Angle(180, 92, -10), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["powerprongs"] = { type = "Model", model = "models/sw_battlefront/props/powerprongs/power_prongs.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "powerpack", pos = Vector(0, 0.5, -0.331), angle = Angle(0, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11b_stock"] = { type = "Model", model = "models/sw_battlefront/props/e11b_stock/e11b_stock.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-5.901, 1.7, -1), angle = Angle(-7, 3, 180), size = Vector(0.899, 1, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11r_stock"] = { type = "Model", model = "models/sw_battlefront/props/e11r_stock/e11r_stock.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-6.801, 1.5, -1), angle = Angle(180, 90, -8.183), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["ironsight"] = { type = "Model", model = "models/sw_battlefront/props/e11r_scope/e11r_scope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3, 1.5, -4.801), angle = Angle(0, -90, 169.481), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11r_grip"] = { type = "Model", model = "models/sw_battlefront/props/e11r_grip/e11r_grip.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "e11_grip", pos = Vector(0, -3.27, 0), angle = Angle(0, 0, 1), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false }
}

SWEP.VElements = {
	["scope_beta"] = { type = "Model", model = "models/sw_battlefront/props/e11_scope/e11_scope.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.1, -3.1, 3.4), angle = Angle(0, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, bonermerge = true, active = false},
	["scope_beta_rt"] = { type = "Model", model = "models/rtcircle.mdl", bone = "E11_GUN", rel = "scope_beta", pos = Vector(-3.03, -.06, 0.25), angle = Angle(180, 0, 180), size = Vector(0.35, 0.35, 0.35), color = Color(255, 255, 255, 255), surpresslightning = false, material = "!tfa_rtmaterial", skin = 0, bodygroup = {}, bonermerge = true, active = false },
	["flashlight"] = { type = "Model", model = "models/sw_battlefront/props/flashlight/flashlight.mdl", bone = "E11_GUN", rel = "", pos = Vector(-1.9, 5.4, 1), angle = Angle(0, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11bstock"] = { type = "Model", model = "models/sw_battlefront/props/e11b_stock/e11b_stock.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.159, -13, -0.931), angle = Angle(6, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["ironsight"] = { type = "Model", model = "models/sw_battlefront/props/e11r_scope/e11r_scope.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.13, -3, 2.93), angle = Angle(0, .5, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["powerprongs"] = { type = "Model", model = "models/sw_battlefront/props/powerprongs/power_prongs.mdl", bone = "E11_GUN", rel = "powerpack", pos = Vector(0, 1, -0.301), angle = Angle(0, -90, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["powerpack"] = { type = "Model", model = "models/sw_battlefront/props/powerpack/power_pack.mdl", bone = "E11_GUN", rel = "", pos = Vector(1.659, -1, 2.299), angle = Angle(0, 0, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["e11rgrip"] = { type = "Model", model = "models/sw_battlefront/props/e11r_grip/e11r_grip.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.17, -2.5, 0.27), angle = Angle(0, 0, 2), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["grip1"] = { type = "Model", model = "models/sw_battlefront/props/e11_grip/e11_grip.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.15, 0.8, 0.3), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
	["stock1"] = { type = "Model", model = "models/sw_battlefront/props/e11r_stock/e11r_stock.mdl", bone = "E11_GUN", rel = "", pos = Vector(0.129, -13.141, 0.699), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false }
}

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "e11_scope", "ironsight" }, order = 1, sel = 2 },
	[2] = { offset = { 0, 0 }, atts = { "green_ammo","blue_ammo","green_ammo_dissolve"}, order = 2 },
	[3] = { offset = { 0, 0 }, atts = { "flashlight"}, order = 3 },
	[4] = { offset = { 0, 0 }, atts = { "stock1","e11bstock" }, order = 4 },
	[5] = { offset = { 0, 0 }, atts = { "grip1","e11rgrip"}, order = 5 },
	[6] = { offset = { 0, 0 }, atts = { "powerpack"}, order = 6 },
	

}

SWEP.IronSightsPos_E11 = Vector(-3.9, -8, 1.5)
SWEP.IronSightsAng_E11 = Vector(0, 0, 1)
SWEP.AttachmentDependencies = {["scope_beta"] = {"scope_beta_rt"}}


DEFINE_BASECLASS( SWEP.Base )